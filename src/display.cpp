#include <iostream>
#include <GL/glew.h>

#include "display.h"

Display::Display(int width, int height, const std::string &title)
{
    // initialize SDL
    SDL_Init(SDL_INIT_EVERYTHING);

    // set attributes
    // 32 bit value colours
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

    // data allocated for every pixel
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);

    // secondary buffer
    // the idea being is that the window is never displaying something that
    // is in the process of being drawn, instead swapping complete buffers
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // create window
    m_window = SDL_CreateWindow(
        title.c_str(),
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
        SDL_WINDOW_OPENGL);

    // create context to allow OpenGL to draw in our window
    m_gl_context = SDL_GL_CreateContext(m_window);

    // init GLEW
    GLenum status = glewInit();
    if (status != GLEW_OK){
        std::cerr << "Glew failed to init!" << std::endl;
    }

    // window is open
    m_closed = false;
}

Display::~Display()
{
    // deinit SDL
    SDL_GL_DeleteContext(m_gl_context);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

void Display::clear(float r, float g, float b, float a)
{
    // set color
    glClearColor(r, g, b, a);
    glClear(GL_COLOR_BUFFER_BIT); // clear the buffer and fill with colour ^
}

void Display::update()
{
    SDL_GL_SwapWindow(m_window);

    // handle OS event
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
        if (SDL_QUIT == e.type) {
            m_closed = true;
        }
    }
}

bool Display::is_closed()
{
    return m_closed;
}