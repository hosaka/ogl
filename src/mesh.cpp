#include <vector>

#include "mesh.h"

Mesh::Mesh(Vertex *vertices, unsigned int num_vert)
{
    // amount of vertices to draw
    m_draw_count = num_vert;

    // generate array of vertices
    glGenVertexArrays(1, &m_vertex_array_obj);

    // use vertex array, bind it, further operations will affect the array
    glBindVertexArray(m_vertex_array_obj);

    // create two vectors for the vertex data stored in the mesh
    std::vector<glm::vec3> positions; // vertex positions
    std::vector<glm::vec2> textures; // texture coordinates

    // reserve the space for data in advance
    positions.reserve(num_vert);
    textures.reserve(num_vert);

    for (unsigned int i = 0; i < num_vert; i++) {
        positions.push_back(*vertices[i].get_pos());
        textures.push_back(*vertices[i].get_tex());
    }
    // vertex buffer, is the way OpenGL refers to data
    // this will give us blocks of data on the GPU for all the buffers to gen
    glGenBuffers(NUM_BUFFERS, m_vertex_array_buf);

    // bind a buffer and how to interpret that buffer as ARRAY_BUFFER
    // this is going to store all of our vertices
    glBindBuffer(GL_ARRAY_BUFFER, m_vertex_array_buf[POSITION_VB]);

    // take the data and put the buffer on the GPU
    // size of a vertex times the number of vertices is the size we need
    // draw hint helps OpenGL to allocate the data buffer correctly
    glBufferData(GL_ARRAY_BUFFER, num_vert * sizeof(positions[0]),&positions[0],
                 GL_STATIC_DRAW);

    // how to interpret the data when drawing
    // this reflects the attributes of our Vertex Class, like position
    // when GPU draws the data, it needs to interpret the attributes correctly
    // that are stored in the Vertex structure
    glEnableVertexAttribArray(0);
    // attributes, index: 0, size, type, normal., skipping, start offset
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // set up another buffer for texture coordinates, just like above, but
    // taking the data from the textures vector
    glBindBuffer(GL_ARRAY_BUFFER, m_vertex_array_buf[TEXTURE_VB]);
    glBufferData(GL_ARRAY_BUFFER, num_vert * sizeof(textures[0]), &textures[0],
                 GL_STATIC_DRAW);
    // new index: 1 bound to texture coordinate data
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0); // only 2 elements

    // stop binding, any further operations will not affect the array, index: 0
    glBindVertexArray(0);
}

void Mesh::draw()
{
    // start working with array
    glBindVertexArray(m_vertex_array_obj);

    // draw array of data (all of our vertices)
    glDrawArrays(GL_TRIANGLES, 0, m_draw_count);

    // stop working with array
    glBindVertexArray(0);
}

Mesh::~Mesh()
{
    glDeleteVertexArrays(1, &m_vertex_array_obj);
}
