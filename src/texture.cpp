#include <iostream>

#include "texture.h"
#include "SOIL/SOIL.h"

Texture::Texture(const std::string &fname) {
    int width, height;

    // load the image using the Simple OpenGL Image Loader library
    unsigned char *image = SOIL_load_image(fname.c_str(), &width, &height, 0,
                                           SOIL_LOAD_RGB);

    // check the image data
    if (NULL == image) {
        std::cerr << "Error: SOIL failed to load: " << fname << std::endl;
    }

    // generate space (buffer) for 1 texture and get the reference to it
    glGenTextures(1, &m_texture);

    // bind our texture as a simple 2D texture
    glBindTexture(GL_TEXTURE_2D, m_texture);


    // set up some texture parameters, like wrapping, so this will repeat (
    // tile) the small texture on x and y, you can also clamp it with black
    // etc.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // if the texture doesn't match the size of the mesh, in this case it
    // will minify or magnify the image, interpolating it linearly, so the
    // texture will scale
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // send the texture to the GPU to draw it
    // texture type: just like previous
    // texture level: for mipmapping (different scales drawn at diff distance)
    // internal format: how the pixels formatted/stored on GPU, basic is RGBA
    // width/height: of the loaded image
    // border: 1 pixel border around the image
    // format: format the current image is in, such as RGBA
    // type: data type of the image represented, note the way SOIL reads the img
    // pixels: pointer to the image data read
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, image);

    // free the image when we're done
    SOIL_free_image_data(image);
}

void Texture::bind(unsigned int unit) {

    // should insert an assertion here to change the range of the unit is
    // >= 0 && <= 31, because OpenGL only allows this many textures

    // change which texture (unit) we're working with (offset from default 0
    // texture, because they're sequential)
    glActiveTexture(GL_TEXTURE0 + unit);

    // bind the texture, so all further operations will use our m_texture
    glBindTexture(GL_TEXTURE_2D, m_texture);
}

Texture::~Texture() {
    // delete the texture once we're done
    glDeleteTextures(1, &m_texture);
}
