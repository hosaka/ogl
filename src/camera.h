#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

// Camera and world perspective class
class Camera {
public:
    // z_near and z_far is the closest and the furthest distance we can see
    // because we don't want to scale infinitely far, or infinitely close
    Camera(const glm::vec3 &pos, float fov, float aspect, float z_near,
           float z_far)
    {
        // set up our perspective value
        m_projection = glm::perspective(fov, aspect, z_near, z_far);

        // this is not going to have an update method, unlike the transform
        // class because our perspective is not going to change much, unless
        // the window gets resized, in which case you might want to update it.

        // camera positioning
        m_position = pos;

        // rotation is going to be hardcoded, but can be passed in
        // this depends on the scene setting
        m_forward = glm::vec3(0.0f, 0.0f, 1.f); // straight into screen, z axis
        m_up = glm::vec3(0.0f, 1.0f, 0.0f); // up is on the y axis
    }

    inline glm::mat4 get_view_projection() const
    {
        // GLM provides a function for looking at things
        // pos: starting from position
        // at: looking at
        // view is perspective times new
        return m_projection * glm::lookAt(m_position, m_position + m_forward, m_up);
    }

protected:
private:
    glm::mat4 m_projection; // camera perspective/projection
    glm::vec3 m_position; // camera position

    glm::vec3 m_forward; // camera rotation with 2 vectors
    glm::vec3 m_up;
};

#endif // CAMERA_H
