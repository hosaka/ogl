#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <GL/glew.h>

#include "transform.h"
#include "camera.h"

class Shader {
    public:
        Shader(const std::string &fname);

        // set the GPU in a state to use the vertex Shader
        void bind();

        // update all uniforms with transformation values and send to GPU
        // also update from the camera instance
        void update(const Transform &transform, const Camera &camera);

        virtual ~Shader();
    protected:
    private:
        // number of shaders, but could have enum
        static const unsigned int NUM_SHADERS = 2;

        // transformation uniforms
        enum {
            TRANSFORM_U,
            NUM_UNIFORM
        };

        // keep track of where the program is, reference to our Shader
        // as well as buffer for shaders and uniforms that we use
        GLuint m_program;
        GLuint m_shaders[NUM_SHADERS];
        GLuint m_uniforms[NUM_UNIFORM];

        // shader creator, loader and error check
        GLuint create_shader(const std::string &text, unsigned int type);
        std::string load_shader(const std::string &fname);
        void check_shader_error(GLuint shader, GLuint flag, bool is_prog,
                                const std::string &err_msg);
};


#endif // SHADER_H
