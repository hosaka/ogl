#include <iostream>

#include "display.h"
#include "mesh.h"
#include "shader.h"
#include "texture.h"
#include "transform.h"
#include "camera.h"

// GPU Pipeline Stages:
// Data: Mesh, being sent to the CPU pipeline to start processing and
// determine where the vertices are positioned.
// Vertex Shader: Processing the data to determine vertices (triangular) for
// the GPU to draw.
// Rasterization: Connect the vertices and fill the surface area to represent
// a shape, frequently a triangle.
// Fragment Shader: Process the generated surface pixels for RGBA values,
// or use the texture (image) to draw the pixels.
// Output Image: Push the generated image out of the pipeline and draw it on
// the screen, or whatevs.

// Movement:
// Applying a mathematical algorithm on our vertices, displacing them and
// moving around to redraw a different image on screen
// A matrix: represents the transformation pattern used by the Vertex Shader,
// this matrix can be applied before sending the data to the GPU. This is
// being done in parallel on the GPU, so it's faster than serially updating
// our vertex buffers. We don't modify the mesh data, all we have to do is
// draw it again with a different matrix to apply different transformations.

// Camera and Perspective:
// The world camera and positioning with moving is done by the same principle
// as the movement, by applying a transformation matrix to the mesh to
// counter the movement direction of the camera. For example if the camera is
// moving or rotating to the left, the world mesh is going to simulate that
// by performing a counter action of moving or rotating to the right.
int main(int argc, char *argv[])
{
    // create a display
    Display display(800, 600, "oh hai");

    // load shader
    Shader shader("../res/shader");

    // load a texture
    Texture texture("../res/bricks.jpg");

    // create a transformation object
    Transform transform;
    float counter = 0.0f;

    // create a camera
    // 3 steps down on z axis
    // 70 degree FOV angle (approx of human eye)
    // display width and height (should really be a getter from display) center
    // z_near and z_far approx close and far distance (approx)
    // these parameters can be updated, just like the transformation instance
    // by calling an update method, if it was implemented, allowing us to
    // move the camera in perspective to the mesh on the screen, or for
    // example bind it to the WASD keys or something
    Camera camera(glm::vec3(0, 0, -3), 70.0f, 800.0f/600.0f, 0.01f, 1000.0f);

    // array of vertices that we can draw using the Mesh class
    // OpenGL (screen/window/texture) coordinates range from -1 to 1: x y z
    //    1
    // -1   1
    //   -1
    Vertex vertices[] = {
            //                 x     y   z,  texture coords (of image)
            Vertex(glm::vec3(-0.5, -0.5, 0), glm::vec2(0.0, 0.0)),
            Vertex(glm::vec3(   0,  0.5, 0), glm::vec2(0.5, 1.0)),
            Vertex(glm::vec3( 0.5, -0.5, 0), glm::vec2(1.0, 0.0))
    };

    // create a Mesh with the number of elements
    Mesh mesh(vertices, sizeof(vertices)/sizeof(vertices[0]));

    // continuously update the display
    while (!display.is_closed()) {
        // set display window colour
        display.clear(0.0f, 0.15f, 0.3f, 1.0f);

        // adjust transformation matrix
        // since we're not getting by reference, we can't modify values directly
        glm::vec3 pos = transform.get_pos();
        glm::vec3 rot = transform.get_rot();

        float sin_count = sinf(counter);
        float cos_count = cosf(counter);
        pos.x = sin_count;
        // pos.y = cos_count;
        pos.z = cos_count;

        rot.x = counter * 0.5f;
        rot.y = counter * 0.5f;
        rot.z = counter * 0.5f;

        transform.set_pos(pos);
        transform.set_rot(rot);
        // transform.set_scale(glm::vec3(sin_count, sin_count, sin_count));

        // bind the shader for usage
        shader.bind();

        // bind our texture to the mesh to use it when we draw
        texture.bind(0);

        // update the shader with transform matrix before we draw the mesh
        // so all the vertices are going to be multiplied by the transformation
        // matrix and then drawn
        shader.update(transform, camera);

        // draw the mesh using the bound shader
        mesh.draw();

        // redraw the window
        display.update();

        counter += 0.1f;
    }

    return 0;
}