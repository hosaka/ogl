#ifndef DISPLAY_H
#define DISPLAY_H

#include <SDL2/SDL.h>
#include <string>

class Display
{
    public:
        Display(int width, int height, const std::string &title);

        void clear(float r, float g, float b, float a);
        void update();
        bool is_closed();

        virtual ~Display();
    protected:
    private:
        SDL_Window *m_window;
        SDL_GLContext m_gl_context;

        bool m_closed;
        // Display::Display(const Display &other) {}
        // Display& Display::operator=(const Display &rhs) {}
};

#endif // DISPLAY_H