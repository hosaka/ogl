#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>

class Texture {
    public:
        Texture(const std::string &fname);

        // set OpenGL to use the texture, unit (0-31) total textures available
        void bind(unsigned int unit);

        virtual ~Texture();
    protected:
    private:
        // texture reference given by OpenGL
        GLuint m_texture;
};

#endif // TEXTURE_H
