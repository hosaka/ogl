#ifndef MESH_H
#define MESH_H

#include <glm/glm.hpp>
#include <GL/glew.h>

// 3 points: x y z
class Vertex
{
    public:
        Vertex(const glm::vec3 &pos, const glm::vec2 &tex)
        {
            this->pos = pos;
            this->tex = tex;
        }
        glm::vec3* get_pos()
        {
            return &pos;
        }
        glm::vec2* get_tex()
        {
            return &tex;
        }
    protected:
    private:
        glm::vec3 pos; // coord position
        glm::vec2 tex; // texture position mapping
};

class Mesh {
    public:
        Mesh(Vertex *vertices, unsigned int num_vert);

        void draw();

        virtual ~Mesh();
    protected:
    private:
        enum
        {
            POSITION_VB,
            TEXTURE_VB,
            NUM_BUFFERS
        };
        GLuint m_vertex_array_obj; // vertex array buffer obj
        GLuint m_vertex_array_buf[NUM_BUFFERS];
        unsigned int m_draw_count;
};


#endif // MESH_H
