#include <iostream>
#include <fstream>

#include "shader.h"

Shader::Shader(const std::string &fname)
{
    // create new shader program
    m_program = glCreateProgram();

    // start creating shaders, could refer by enum if class is matching
    // two file extensions can be anything, just a convention
    m_shaders[0] = create_shader(load_shader(fname + ".vs"), GL_VERTEX_SHADER);
    m_shaders[1] = create_shader(load_shader(fname + ".fs"), GL_FRAGMENT_SHADER);

    // add all the loaded shaders to program
    for (unsigned int i = 0; i < NUM_SHADERS; i++) {
        glAttachShader(m_program, m_shaders[i]);
    }

    // point to attribute locations
    // this is the position field in our Vertex, and that's what we bind to
    glBindAttribLocation(m_program, 0, "pos");

    // also bind the position of the texture coordinate as a shader attribute
    glBindAttribLocation(m_program, 1, "tex");

    // link the shader to program and check for errors
    glLinkProgram(m_program);
    check_shader_error(m_program, GL_LINK_STATUS, true,
                       "Error: Program linking failed: ");

    // make sure program is valid after linking
    glValidateProgram(m_program);
    check_shader_error(m_program, GL_LINK_STATUS, true,
                       "Error: Program is invalid: ");

    // figure out what handle refers to our transformation uniform, since we
    // called it transform, that's what we look for here
    m_uniforms[TRANSFORM_U] = glGetUniformLocation(m_program, "transform");
}

void Shader::bind()
{
    // bind (use) the Shader
    glUseProgram(m_program);
}

void Shader::update(const Transform &transform, const Camera &camera)
{
    // first get our model (world) with transformation data
    // we also apply our camera transformation model, should really be a
    // separate variable, or model should be renamed but meh
    glm::mat4 model = camera.get_view_projection() * transform.get_model();

    // update our uniforms (our matrix is 4 float values: 4fv)
    // uniform:
    // count: how many items we're sending
    // transpose: glm already has this in correct format
    // value: data we're applying (first address of the matrix)
    glUniformMatrix4fv(m_uniforms[TRANSFORM_U], 1, GL_FALSE, &model[0][0]);
}

Shader::~Shader()
{
    // detach and delete the shaders from the program
    for (unsigned int i = 0; i < NUM_SHADERS; i++) {
        glDetachShader(m_program, m_shaders[i]);
        glDeleteShader(m_shaders[i]);
    }
    // destroy program
    glDeleteProgram(m_program);
}

// create shader
GLuint Shader::create_shader(const std::string &text, unsigned int type)
{
    GLuint shader = glCreateShader(type);

    if (0 == shader) {
        std::cerr << "Error: Shader creation failed" << std::endl;
    }

    // shader name strings and length
    const GLchar *shader_sources[1];
    GLint shader_sources_lengh[1];

    shader_sources[0] = text.c_str();
    shader_sources_lengh[0] = text.length();

    // compile the shader
    glShaderSource(shader, 1, shader_sources, shader_sources_lengh);
    glCompileShader(shader);

    // check for compile errors
    check_shader_error(shader, GL_COMPILE_STATUS, false, "Error: Shader "
            "compilation failed: ");

    return shader;
}

// load the shader file
std::string Shader::load_shader(const std::string &fname)
{
    std::ifstream file;
    std::string output;
    std::string line;

    // open the shader file
    file.open((fname).c_str());

    // read lines from file, adding newline
    if (file.is_open()) {
        while (file.good()) {
            getline(file, line);
            output.append(line + "\n");
        }
    } else {
        std::cerr << "Unable to load shader: " << fname << std::endl;
    }

    return output;
}

// check shader file for error
void Shader::check_shader_error(GLuint shader, GLuint flag, bool is_prog,
                                const std::string &err_msg)
{
    // GL return value
    GLint ret = 0;
    // error message string
    GLchar error[1024] = { 0 };

    // check if the shader is successfully loaded
    if (is_prog) {
        glGetProgramiv(shader, flag, &ret);
    } else {
        glGetShaderiv(shader, flag, &ret);
    }

    if (GL_FALSE == ret) {
        // fetch error message from log
        if (is_prog) {
            glGetProgramInfoLog(shader, sizeof(err_msg), NULL, error);
        } else {
            glGetShaderInfoLog(shader, sizeof(err_msg), NULL, error);
        }

        // print error message
        std::cerr << err_msg << ": '" << error << "'" << std::endl;
    }
}
