#ifndef TRANSFORM_H
#define TRANSFORM_H

// matrix and maths
#include "glm/glm.hpp"
// transformation functions
#include "glm/gtx/transform.hpp"

class Transform {
public:
    // transformation parameters are:
    // position, rotation, scale
    Transform(const glm::vec3 &pos = glm::vec3(),
              const glm::vec3 &rot = glm::vec3(),
              const glm::vec3 &scale = glm::vec3(1.0f, 1.0f, 1.0f)) :
            m_pos(pos),
            m_rot(rot),
            m_scale(scale) {}

    // convert the transformation parameters into a matrix 4x4
    // generally called a model matrix or a world matrix
    glm::mat4 get_model() const
    {
        // GLM translate functions automatically generates matrix from vectors
        glm::mat4 pos_matrix = glm::translate(m_pos);
        glm::mat4 scale_matrix = glm::scale(m_scale);

        // rotation can be represented with quaternions, rather than simple axis
        // to avoid a gimbal lock, but there is no handy function to convert the
        // vector data in GLM, so we make our own
        glm::mat4 rot_x_matrix = glm::rotate(m_rot.x, glm::vec3(1, 0, 0));
        glm::mat4 rot_y_matrix = glm::rotate(m_rot.y, glm::vec3(0, 1, 0));
        glm::mat4 rot_z_matrix = glm::rotate(m_rot.z, glm::vec3(0, 0, 1));

        // to combine the matrices, we can multiply them, the order matters,
        // it's written out backwards, gen'd code works right to left in the
        // expression
        glm::mat4 rot_matrix = rot_z_matrix * rot_y_matrix * rot_x_matrix;

        // we return only one matrix which represents all the transformations
        // applied, again by multiplying them
        // we want to apply scale, rotation then position, different order is
        // valid but will, for example rotate the object around the world,
        // rather than around it's own axis if the position comes first etc.

        // we get to the appropriate size first
        // then spin the object around
        // and then we position it
        return pos_matrix * rot_matrix * scale_matrix;
    }

    // getters and setters for member variables
    const glm::vec3 &get_pos() const { return m_pos; }
    void set_pos(const glm::vec3 &pos) { m_pos = pos; }

    const glm::vec3 &get_rot() const { return m_rot; }
    void set_rot(const glm::vec3 &rot) { m_rot = rot; }

    const glm::vec3 &get_scale() const { return m_scale; }
    void set_scale(const glm::vec3 &scale) { m_scale = scale; }

    virtual ~Transform();
protected:
private:
// the actual data for transformation
    glm::vec3 m_pos;
    glm::vec3 m_rot;
    glm::vec3 m_scale;
};


#endif //TRANSFORM_H
