### target settings ###

# application
PROG ?= ogl

# compiler and linker
CXX ?= g++
LD ?= ld

# build dir
BUILD ?= build

# link libraries
LIB += \
	-lGL \
	-lGLEW \
	-lSDL2 \
	-lSOIL

# include paths
INC += $(addprefix -I,\
	. \
	include \
	)

# sources list
SRC_CPP += $(addprefix src/, \
    camera.cpp \
    display.cpp \
    main.cpp \
    mesh.cpp \
    shader.cpp \
    texture.cpp \
    transform.cpp \
	)

# compiled objects
OBJ += \
	$(addprefix $(BUILD)/, $(SRC_CPP:.cpp=.o))

# compiler flags
CPPFLAGS += \
	-Wall -Werror \
	-Wpointer-arith \
	-Wuninitialized \

# linker flags
LDFLAGS += \

# default target
all: $(BUILD)/$(PROG)

### build rules ###
$(BUILD)/%.o: %.cpp
	$(ECHO) "CC $<"
	$(Q)$(CXX) -o $@ -c $^ $(CPPFLAGS)

OBJ_DIRS = $(sort $(dir $(OBJ)))
$(OBJ): | $(OBJ_DIRS)
$(OBJ_DIRS):
	$(MKDIR) -p $@

$(BUILD)/$(PROG): $(OBJ)
	$(ECHO) "Linking: $@"
	$(Q)$(CXX) -o $@ $^ $(LIB) $(LDFLAGS)

clean:
	$(RM) -rf $(BUILD) $(OBJ)

.PHONY: all clean debug

### verbosity ###
ifeq ("$(origin V)", "command line")
BUILD_VERBOSE = $(V)
endif
ifndef BUILD_VERBOSE
BUILD_VERBOSE = 0
endif
ifeq ($(BUILD_VERBOSE), 0)
Q = @
else
Q =
endif

### default settings ###
ECHO = @echo
RM = rm
MKDIR = mkdir
