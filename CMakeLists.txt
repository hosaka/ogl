cmake_minimum_required(VERSION 3.3)

project(ogl)

add_compile_options(
        ${CMAKE_CXX_FLAGS}
        -std=c++11
        -Wall -Werror
        -Wpointer-arith
        -Wuninitialized
)

include_directories(
        include
        src
)

set(SOURCE_FILES
        src/camera.cpp
        src/display.cpp
        src/main.cpp
        src/mesh.cpp
        src/shader.cpp
        src/texture.cpp
        src/transform.cpp
)

add_executable(${CMAKE_PROJECT_NAME} ${SOURCE_FILES})
target_link_libraries(${CMAKE_PROJECT_NAME} SDL2 GL GLEW SOIL)