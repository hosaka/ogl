#version 120

// shared varying var from vertex shader with texture coordinates
varying vec2 tex_coord0;

// diffuse sampler...for drawing the pixels i guess?
uniform sampler2D diffuse;

void main()
{
    // we get our texture coordinate (varying tex_coord0) from the vertex shader
    // it updates as it reads the texture automatically, so we map it to our
    // mesh
	gl_FragColor = texture2D(diffuse, tex_coord0); // vec4(1.0, 0.0, 0.0, 1.0);
}