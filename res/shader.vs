#version 120

// our vertex attributes
// vertex position
// and loaded texture position
attribute vec3 pos;
attribute vec2 tex;

// shared vector with texture coordinates
varying vec2 tex_coord0;

// we are gonig to take in the matrix and apply it at a shader level to
// perform the transformation, a uniform variable can be set by the CPU, this
// is where we're going to store our transform matrix
uniform mat4 transform;

void main()
{
	// position our vertices
	// in order to apply the transformation matrix we received, all we need
	// to do is simply multiply our position matrix by the transform matrix

	// the second vec4 param is the magnitude of transformation application,
	// current 1.0 is 1:1 scale, 2.0 will set it to 2:1 scale etc.
	gl_Position = transform * vec4(pos, 1.0);

	// varying attribute is shared across the shader, so we assign it here to
	// get access to it in the fragment shader
	tex_coord0 = tex;
}